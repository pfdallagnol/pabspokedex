import { PokemonType } from "app/types/pokemon.type";

export interface IPokedexLocalStorage {
  caughtList: string[];
  wishList: string[];
}
export interface IPokemonState extends IPokedexLocalStorage {
  pokemonCount: number;
  pokemons: Map<string, PokemonType>;
  loading: boolean;
}
