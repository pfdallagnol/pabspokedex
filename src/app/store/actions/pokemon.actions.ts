import { PokemonType } from "app/types/pokemon.type";

export class GetPokemonList {
  static readonly type: string = "[ Pokemon ] Get Pokemon List";
  constructor() {}
}

export class GetPokemonsDetails {
  static readonly type: string = "[ Pokemon ] Get Pokemons Details";
  constructor(public payload: { namesOrIds: string[] }) {}
}

export class GetPokemon {
  static readonly type: string = "[ Pokemon ] Get Pokemon";
  constructor(public payload: { nameOrId: string }) {}
}

export class PokemonCaught {
  static readonly type: string = "[ Pokemon ] Pokemon Caught";
  constructor(public payload: { pokemon: PokemonType }) {}
}

export class PokemonWishList {
  static readonly type: string = "[ Pokemon ] Pokemon Wishlist";
  constructor(public payload: { pokemon: PokemonType }) {}
}
