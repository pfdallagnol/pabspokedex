import { NgModule } from '@angular/core';
import { NgxsModule } from '@ngxs/store';
import { environment } from 'environments/environment';
import { appState } from './state';

/**
 * @module App
 * @class AppModule
 */
@NgModule({
    providers: [],
    imports: [
        NgxsModule.forRoot(appState, { developmentMode: !environment.production }),
    ]
})
export class StoreTestingModule { }
