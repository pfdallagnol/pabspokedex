import { NgxsModuleOptions } from "@ngxs/store";
import { environment } from "environments/environment";
import { PokemonState } from "./pokemon.state";

export const appState = [
    PokemonState
];

export const appStateOptions: NgxsModuleOptions = {
    developmentMode: !environment.production,
    selectorOptions: {
        suppressErrors: environment.production
    }
};
