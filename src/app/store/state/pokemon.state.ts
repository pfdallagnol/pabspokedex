import { Injectable } from "@angular/core";
import {
  Action,
  createSelector,
  Selector,
  State,
  StateContext,
} from "@ngxs/store";
import { SearchService } from "app/services/search.service";
import { PokemonType } from "app/types/pokemon.type";
import { addNumberPad } from "app/utils/functions/tools";
import { sortBy } from "lodash-es";
import { from, Observable } from "rxjs";
import { mergeMap, tap } from "rxjs/operators";
import {
  GetPokemonList,
  GetPokemonsDetails,
  PokemonCaught,
  PokemonWishList,
} from "../actions/pokemon.actions";
import {
  IPokedexLocalStorage,
  IPokemonState,
} from "../interfaces/pokemon-state.interface";

const defaults: IPokemonState = {
  pokemonCount: 0,
  pokemons: new Map<string, PokemonType>(),
  caughtList: [],
  wishList: [],
  loading: false,
};

@State<IPokemonState>({ name: "Pokemon", defaults })
@Injectable()
export class PokemonState {
  constructor(private _searchService: SearchService) {}

  @Selector()
  static pokemonsLoading(state: IPokemonState): boolean {
    return state.loading;
  }

  static filteredPokemons(query: string) {
    return createSelector(
      [PokemonState],
      (state: IPokemonState): PokemonType[] => {
        return sortBy(Array.from(state.pokemons.values()), ["id"]).filter(
          (pokemon) => pokemon.name.includes(query)
        );
      }
    );
  }

  static filteredPokemonsPerPage(query: string, page = 1) {
    return createSelector(
      [PokemonState],
      (state: IPokemonState): PokemonType[] => {
        return sortBy(Array.from(state.pokemons.values()), ["id"])
          .filter((pokemon) => pokemon.name.includes(query))
          .slice((page - 1) * 50, page * 50);
      }
    );
  }

  @Action(GetPokemonList)
  getPokemonList({
    getState,
    patchState,
  }: StateContext<IPokemonState>): Observable<any> {
    const lcStorage = this._getLocalStorage();
    patchState({ ...getState(), ...lcStorage, loading: true });

    return this._searchService.getPokemonList().pipe(
      tap((pokemonList) => {
        const pokemonsState = getState().pokemons;
        pokemonList.results.forEach((pokemon, index) =>
          pokemonsState.set(pokemon.name, {
            ...pokemon,
            paddedId: "",
            isCaught: !!lcStorage.caughtList?.includes(pokemon.name),
            isWishlisted: !!lcStorage.wishList?.includes(pokemon.name),
            hasFetchDetails: false,
          })
        );

        patchState({
          ...getState(),
          pokemonCount: pokemonList.count,
          loading: false,
        });
      })
    );
  }

  @Action(GetPokemonsDetails)
  getPokemonsDetails(
    { getState, patchState }: StateContext<IPokemonState>,
    { payload }: GetPokemonsDetails
  ): Observable<any> {
    return from(payload.namesOrIds).pipe(
      mergeMap((nameOrId) =>
        this._searchService.getPokemonDetails(nameOrId).pipe(
          tap((pokemonDetail) => {
            const pokemons = getState().pokemons;
            pokemons.set(pokemonDetail.name, {
              ...pokemons.get(pokemonDetail.name),
              ...pokemonDetail,
              hasFetchDetails: true,
              primaryImg:
                pokemonDetail.sprites.front_default ??
                pokemonDetail.sprites.front_shiny ??
                "", // Some pokemons have different ids than the index
              paddedId: addNumberPad(pokemonDetail.id.toString(), 3, "0"), // Some pokemons have different ids than the index
            });

            patchState({
              ...getState(),
            });
          })
        )
      )
    );
  }

  @Action(PokemonCaught)
  pokemonCaught(
    { getState, patchState }: StateContext<IPokemonState>,
    { payload }: PokemonCaught
  ): void {
    const pokemon = getState().pokemons.get(payload.pokemon.name);
    if (pokemon) {
      pokemon.isCaught = !payload.pokemon.isCaught;
      const caughtList = pokemon.isCaught
        ? [...getState().caughtList, pokemon.name]
        : getState().caughtList.filter((item) => item !== pokemon.name);
      patchState({ ...getState(), caughtList });
      this._setLocalStorage(getState());
    }
  }

  @Action(PokemonWishList)
  pokemonWishlist(
    { getState, patchState }: StateContext<IPokemonState>,
    { payload }: PokemonWishList
  ): void {
    const pokemon = getState().pokemons.get(payload.pokemon.name);
    if (pokemon) {
      pokemon.isWishlisted = !payload.pokemon.isWishlisted;
      const wishList = pokemon.isWishlisted
        ? [...getState().wishList, pokemon.name]
        : getState().wishList.filter((item) => item !== pokemon.name);
      patchState({ ...getState(), wishList });
      this._setLocalStorage(getState());
    }
  }

  private _setLocalStorage(state: IPokemonState): void {
    localStorage.setItem(
      "pabspokedex",
      JSON.stringify({
        caughtList: state.caughtList,
        wishList: state.wishList,
      })
    );
  }

  private _getLocalStorage(): IPokedexLocalStorage {
    return JSON.parse(localStorage.getItem("pabspokedex") ?? "{}");
  }
}
