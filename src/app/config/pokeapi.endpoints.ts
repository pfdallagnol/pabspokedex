const base = 'https://pokeapi.co/api/v2';

interface IApiEndpoints {
    [name: string]: string;
}

export const PokeApiEndpoints: IApiEndpoints = {
    // SEARCH
    pokemon: `${base}/pokemon`
}
