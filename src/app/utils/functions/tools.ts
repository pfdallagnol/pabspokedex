export const addNumberPad = (value: string, length: number, char: string = '0'): string => {
    return value.length > length ? value : (char.repeat(length - value.length) + value);
};

export const isNullOrUndefined = (value: any): boolean => {
    return value === null || value === undefined;
};
