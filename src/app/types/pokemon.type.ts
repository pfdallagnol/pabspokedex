import { IPokemon, IPokemonBasicInfo } from "app/interfaces/pokemon.interface";

export type PokemonType = IPokemonBasicInfo | IPokemon;
