import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { PageEvent } from "@angular/material/paginator";
import { Select, Store } from "@ngxs/store";
import {
  GetPokemonList,
  GetPokemonsDetails,
  PokemonCaught,
  PokemonWishList,
} from "app/store/actions/pokemon.actions";
import { PokemonState } from "app/store/state/pokemon.state";
import { PokemonType } from "app/types/pokemon.type";
import { Observable, ReplaySubject } from "rxjs";
import { delay, map, skipWhile, take, takeUntil } from "rxjs/operators";

@Component({
  selector: "app-search",
  styleUrls: ["./search.component.scss"],
  template: `
    <div class="search-list">
      <mat-progress-bar
        *ngIf="isLoading$ | async"
        mode="indeterminate"
      ></mat-progress-bar>

      <div class="search">
        <h2>Pokemon Finder</h2>
        <input
          type="text"
          matInput
          [formControl]="searchControl"
          placeholder="Ex. ditto"
        />

        <!-- No Results -->
        <div
          *ngIf="
            searchControl?.value.length > 1 &&
            !(filteredPokemons$ | async)?.length
          "
          class="no-results"
        >
          <h4>Oops! No Pokemons Found</h4>
        </div>
      </div>

      <div class="list">
        <div class="list-wrapper">
          <ng-container
            *ngFor="let pokemon of filteredPokemonsPerPage$ | async"
          >
            <app-pokemon-card
              [pokemon]="pokemon"
              (pokemonCaught)="onPokemonCaught($event)"
              (pokemonWishlist)="onPokemonWishlist($event)"
            >
            </app-pokemon-card>
          </ng-container>
        </div>

        <mat-paginator
          *ngIf="pokemonsCount$ | async"
          [length]="pokemonsCount$ | async"
          [pageSize]="50"
          [hidePageSize]="true"
          [pageIndex]="page - 1"
          (page)="paginate($event)"
        >
        </mat-paginator>
      </div>
    </div>
  `,
})
export class SearchComponent implements OnInit, OnDestroy {
  @Select(PokemonState.pokemonsLoading)
  isLoading$?: Observable<boolean>;

  _destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  searchControl = new FormControl("");
  page: number = 1;

  filteredPokemons$: Observable<PokemonType[]> = this._store.select(
    PokemonState.filteredPokemons("".toLocaleLowerCase())
  );
  pokemonsCount$: Observable<number> = this.filteredPokemons$.pipe(
    map((pokemons) => pokemons.length)
  );
  filteredPokemonsPerPage$: Observable<PokemonType[]> =
    this.filteredPokemons$.pipe(
      map((pokemons) => pokemons.slice((this.page - 1) * 50, this.page * 50))
    );

  constructor(private _store: Store) {}

  ngOnInit(): void {
    this._store.dispatch(new GetPokemonList());
    this._listenForInputEvent();

    this._fetchDetails();
  }

  ngOnDestroy() {
    this._destroyed$.next(true);
    this._destroyed$.complete();
  }

  paginate(page: PageEvent) {
    this.page = page.pageIndex + 1;
    this._doPagingSplit();
    this._fetchDetails();
  }

  private _listenForInputEvent(): void {
    this.searchControl.valueChanges
      .pipe(takeUntil(this._destroyed$), delay(500))
      .subscribe((input) => {
        this.page = 1;
        this.filteredPokemons$ = this._store.select(
          PokemonState.filteredPokemons((input ?? "").toLocaleLowerCase())
        );

        this._doPagingSplit();

        this._fetchDetails();
      });
  }

  private _doPagingSplit(): void {
    this.filteredPokemonsPerPage$ = this.filteredPokemons$.pipe(
      map((pokemons) => pokemons.slice((this.page - 1) * 50, this.page * 50))
    );

    this.pokemonsCount$ = this.filteredPokemons$.pipe(
      map((pokemons) => pokemons.length)
    );
  }

  private _fetchDetails(): void {
    this.filteredPokemonsPerPage$
      .pipe(
        skipWhile((pokemons) => !pokemons.length),
        takeUntil(this._destroyed$),
        take(1),
        map((pokemons) =>
          pokemons.filter((pokemon) => !pokemon.hasFetchDetails)
        )
      )
      .subscribe((pokemonsWithoutDetail) => {
        this._store.dispatch(
          new GetPokemonsDetails({
            namesOrIds: pokemonsWithoutDetail.map((pokemon) => pokemon.name),
          })
        );
      });
  }

  onPokemonCaught(pokemon: PokemonType): void {
    this._store.dispatch(new PokemonCaught({ pokemon }));
  }

  onPokemonWishlist(pokemon: PokemonType): void {
    this._store.dispatch(new PokemonWishList({ pokemon }));
  }
}
