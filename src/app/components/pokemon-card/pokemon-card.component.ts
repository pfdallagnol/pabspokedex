import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from "@angular/core";
import { PokemonType } from "app/types/pokemon.type";

@Component({
  selector: "app-pokemon-card",
  styleUrls: ["./pokemon-card.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <div *ngIf="pokemon" class="card">
      <img
        *ngIf="!imageHasError"
        [src]="pokemon.primaryImg"
        [alt]="pokemon.name"
        height="180"
        width="180"
        loading="lazy"
        (error)="setImageError()"
      />

      <img
        *ngIf="imageHasError"
        [src]="fallbackImage"
        alt="Missing image - psyduck"
        height="180"
        width="180"
        loading="lazy"
      />

      <span class="id">#{{ pokemon.paddedId }}</span>
      <span class="name">{{ pokemon.name }}</span>

      <ng-container *ngIf="pokemon.types?.length">
        <div class="types">
          <span
            *ngFor="let type of pokemon.types"
            class="pill"
            [ngClass]="type.type.name"
            >{{ type.type.name }}</span
          >
        </div>
      </ng-container>

      <div class="actions">
        <span class="caught">
          <ng-container
            *ngTemplateOutlet="
              dynamicTooltip;
              context: {
                context: {
                  isCondition: pokemon.isCaught,
                  text1: 'Remove from the Pokemons you caught',
                  text2: 'Add to the Pokemons you caught'
                }
              }
            "
          >
          </ng-container>
          <svg
            (mouseenter)="isCaughtHovered = true"
            (mouseleave)="isCaughtHovered = false"
            (click)="onClickCaught(pokemon)"
          >
            <use
              *ngIf="isCaughtHovered"
              [attr.xlink:href]="
                getPokeIconPath(
                  pokemon.isCaught ? 'pokeball-empty' : 'pokeball-full'
                )
              "
            ></use>
            <use
              *ngIf="!isCaughtHovered"
              [attr.xlink:href]="
                getPokeIconPath(
                  pokemon.isCaught ? 'pokeball-full' : 'pokeball-empty'
                )
              "
            ></use>
          </svg>
        </span>

        <span class="wishlist">
          <ng-container
            *ngTemplateOutlet="
              dynamicTooltip;
              context: {
                context: {
                  isCondition: pokemon.isWishlisted,
                  text1: 'Remove from Wishlist',
                  text2: 'Add to Wishlist'
                }
              }
            "
          >
          </ng-container>
          <svg
            (mouseenter)="isWishlistedHovered = true"
            (mouseleave)="isWishlistedHovered = false"
            (click)="onClickWishlist(pokemon)"
          >
            <use
              *ngIf="isWishlistedHovered"
              [attr.xlink:href]="
                getPokeIconPath(
                  pokemon.isWishlisted ? 'star-empty' : 'star-full'
                )
              "
            ></use>
            <use
              *ngIf="!isWishlistedHovered"
              [attr.xlink:href]="
                getPokeIconPath(
                  pokemon.isWishlisted ? 'star-full' : 'star-empty'
                )
              "
            ></use>
          </svg>
        </span>
      </div>
    </div>

    <ng-template #dynamicTooltip let-templateContext="context">
      <span class="tooltip">{{
        templateContext.isCondition
          ? templateContext.text1
          : templateContext.text2
      }}</span>
    </ng-template>
  `,
})
export class PokemonCardComponent {
  @Input()
  pokemon?: PokemonType;

  @Output()
  pokemonCaught: EventEmitter<PokemonType> = new EventEmitter<PokemonType>();

  @Output()
  pokemonWishlist: EventEmitter<PokemonType> = new EventEmitter<PokemonType>();

  fallbackImage: string = "/assets/images/psyduck_confuse.gif";
  imageHasError: boolean = false;
  isCaughtHovered: boolean = false;
  isWishlistedHovered: boolean = false;
  private _pokeIconsDef: string = "/assets/images/pokeicons-defs.svg";

  getPokeIconPath(icon: string): string {
    return `${this._pokeIconsDef}#icon-${icon}`;
  }

  setImageError() {
    this.imageHasError = true;
  }

  onClickCaught(pokemon: PokemonType): void {
    this.pokemonCaught.emit(pokemon);
  }

  onClickWishlist(pokemon: PokemonType): void {
    this.pokemonWishlist.emit(pokemon);
  }
}
