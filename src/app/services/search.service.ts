import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { PokeApiEndpoints } from "app/config/pokeapi.endpoints";
import { IBEPokemonList, IPokemon } from "app/interfaces/pokemon.interface";
import { Observable } from "rxjs";

@Injectable({ providedIn: "root" })
export class SearchService {
  constructor(private _http: HttpClient) {}

  getPokemonList(
    params: { limit: string; offset: string } = { limit: "2000", offset: "0" }
  ): Observable<IBEPokemonList> {
    const httpParams = new HttpParams({ fromObject: params });

    return this._http.get(PokeApiEndpoints["pokemon"], {
      params: httpParams,
    }) as Observable<IBEPokemonList>;
  }

  getPokemonDetails(nameOrId: string): Observable<IPokemon> {
    return this._http.get(
      `${PokeApiEndpoints["pokemon"]}/${nameOrId}`
    ) as Observable<IPokemon>;
  }
}
