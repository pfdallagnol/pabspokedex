export interface IPokemonTypes {
  slot: number;
  type: IPokemonListProperty;
}

export interface IPokemonListProperty {
  name: string;
  url: string;
}

export interface IPokemonBasicInfo extends IPokemonListProperty {
  name: string;
  url: string;
  primaryImg?: string;
  paddedId: string;
  types?: IPokemonTypes[];
  isCaught: boolean;
  isWishlisted: boolean;
  hasFetchDetails: boolean;
}

export interface IPokemon extends IPokemonBasicInfo {
  id: number;
  abilities: Object[];
  moves: Object[];
  past_types: [];
  species: Object;
  sprites: {
    front_default: string;
    front_shiny: string;
  };
  stats: Object[];
  types: IPokemonTypes[];
  forms: Object[];
  game_indices: Object[];
  held_items: Object[];
  base_experience: number;
  height: number;
  weight: number;
  is_default: boolean;
  order: number;
  location_area_encounters: string;
}

export interface IBEPokemonList {
  count: number;
  next: string;
  previous: string;
  results: IPokemonListProperty[];
}
