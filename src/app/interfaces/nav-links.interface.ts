
export type NavLinkNameType = 'Dashboard' | 'SourceBots' | 'Intel' | 'Companies' | 'Pipelines';

export interface INavLink {
    name: NavLinkNameType;
    icon: string;
    urlLink?: string[];
}
